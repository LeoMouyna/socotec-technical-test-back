from django.db import models


class Actor(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    birth_date = models.DateField()

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class GradeChoices(models.IntegerChoices):
    DISASTROUS = 1, 'Disastrous'
    PASSABLE = 2, 'Passable'
    HONORABLE = 3, 'Honorable'
    EXCELLENT = 4, 'Excellent'
    BRILLIANT = 5, 'Brillant'


class Review(models.Model):
    grade = models.IntegerField(
        default=GradeChoices.PASSABLE, choices=GradeChoices.choices)


class Movie(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField()
    actors = models.ManyToManyField(Actor)
    reviews = models.ForeignKey(Review, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.title
