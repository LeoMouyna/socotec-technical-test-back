from rest_framework import viewsets
from api.serializer import ActorSerializer, ReviewSerializer, MovieSerializer
from api.models import Actor, Review, Movie
from django.contrib.auth.models import User


class ActorViewSet(viewsets.ModelViewSet):
    """
    API endpoint for actors.
    """
    queryset = Actor.objects.all()
    serializer_class = ActorSerializer


class ReviewViewSet(viewsets.ModelViewSet):
    """
    API endpoint for reviews.
    """
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer


class MovieViewSet(viewsets.ModelViewSet):
    """
    API endpoint for movies.
    """
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
